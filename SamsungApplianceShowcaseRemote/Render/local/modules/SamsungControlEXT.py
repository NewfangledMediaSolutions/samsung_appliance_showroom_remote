"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions
import socket
import sys
import TDDelay

class SamsungControl:
	"""
	SamsungControl description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp
		self.DB = op.RenderDB
		# attributes:
		self.a = 0 # attribute
		self.B = 1 # promoted attribute
		#self.ContentDB = op.ContentDB
		# properties
		TDF.createProperty(self, 'MyProperty', value=0, dependable=True,
						   readOnly=False)

		# stored items (persistent across saves and re-initialization):
		storedItems = [
			# Only 'name' is required...
			{'name': 'StoredProperty', 'default': None, 'readOnly': False,
			 						'property': True, 'dependable': True},
		]
		# Uncomment the line below to store StoredProperty. To clear stored
		# 	items, use the Storage section of the Component Editor
		
		# self.stored = StorageManager(self, ownerComp, storedItems)

	def myFunction(self, v):
		debug(v)

	def PromotedFunction(self, v):
		debug(v)

	def GetControllerName(self):
		return socket.gethostname()

	def GetIPAddr(self):
		return socket.gethostbyname(self.GetControllerName())


	def SamsungClickEvent(self):
		pickPath = str(op.GUI.op('interact/pickPath')['path', 1])
		mainPage = op.GUI.op('interact').fetch('MAIN_PAGES', [])

		# print('Clicked on {}'.format(pickPath))


		# sceneName = pickPath.split('/')[2]
		screenName = op.GUI.fetch('CURRENT_ROOM', None)
		buttonName = pickPath.split('/')[-2]
		if buttonName == 'audio':
			op.GUI.op('interact/renderpick1').par.strategy = 0
		else:
			op.GUI.op('interact/renderpick1').par.strategy = 1
		settingsScreen = op.GUI.fetch('SettingsScreen', None)

		actionButtons = ['jump', 'pause']
		roomNames = [i for i in self.DB.op('mainPages').col('name')[1:]]

		if buttonName in actionButtons:
			if buttonName == 'pause':
				cState = op.GUI.fetch('PAUSE', 0)
				cVal = 1 - cState
				op.GUI.store('PAUSE', cVal)
				self.PauseLightingCue(cVal)
				op.COMM.MoviePlayerUDP.send('{} {} {}'.format(op.GUI.fetch('CURRENT_ROOM'), buttonName.capitalize(), str(cVal)))
			elif buttonName == 'jump':
				op.COMM.MoviePlayerUDP.send('{} {} {}'.format(op.GUI.fetch('CURRENT_ROOM'), buttonName.capitalize(), (-5)))

		elif buttonName == 'mainMenu':
			op.PageButtons.op('sourceNone').click()
			self.ownerComp.store('CURRENT_ROOM', None)
			self.ShowVideoScrubber()
			self.ShowAudioControl()
			# self.RecallPreset(3)


		elif buttonName == 'audio':
			ty = op.GUI.op('interact/renderpick1')['ty']
			bar = op(pickPath).parent(2).op('bar')

			cVal = ty.eval() + .5
			bar.par.sy = cVal
			self.AudioLevels(cVal)



		elif buttonName in roomNames:
			buttonNum = roomNames.index(buttonName)
			# op.RoomSelector.clickChild(buttonNum)
			op.COMM.SwitchCommPorts(buttonName)
			self.SwitchPage(buttonName)
			self.ShowAudioControl(1)
			cVal = op.GUI.fetch('AUDIO_LEVELS').get(buttonName)
			ty = op.GUI.op('interact/renderpick1')['ty']
			bar = op.GUI.op('familyHub/interactiveMap/audio/bar')
			self.RecallPreset(1)

			bar.par.sy = cVal


		elif 'chapter' in buttonName:
			self.SwitchContent(button=buttonName)


	def SwitchPage(self, roomName):
		currentRoom = self.ownerComp.fetch('CURRENT_ROOM', None)
		if currentRoom != roomName:
			roomIDX = self.DB.op('mainPages').col('name').index(roomName)
			op.PageButtons.clickChild(roomIDX-1)
			self.ownerComp.store('CURRENT_ROOM', roomName)
			self.ownerComp.op('textures/nameSwitch').par.index = roomIDX-1


	def SwitchContent(self, button=None, screen=None):

		print('Switching Content:  ', button)
		room = op.GUI.fetch('CURRENT_ROOM', None)
		msg = '{} {}'.format(room, button)

		op.COMM.op('lightCueManager').run(button, room)
		op.COMM.MoviePlayerUDP.send(msg)
		# self.ShowVideoScrubber()

		lastVideoData = op.GUI.fetch(room, {})
		lastVideo = lastVideoData.get('')
		op.ScrubberFadeButton.click(1)
		TDDelay.Delay(delayFrames=120, fromOP=me).Call(self, 'ShowVideoScrubber', button)
		op.ChapterButtons.clickChild(tdu.digits(button)-1)
		# self.ShowVideoScrubber(button)
		return

	def OpenSettingsScreen(self, button=None, screen=None):
		# load settings into settings page
		settingsPage = button.split('Settings')
		op.GUI.op('settings/interactiveMap/setup').run(screen)

		# print(button, screen, settingsDat)
		# self.SwitchContent(button=button, screen=screen)
		op.GUI.store('ACTIVE_SCREEN', 'settings')
		op.GUI.store('SettingsScreen', screen)
		
	def SendLightingCue(self, cueNum=None, button=None, screen=None):
		
		pass

	def ShowVideoScrubber(self, chapter=None):
		if chapter is None:
			op.ChapterButtons.op('sourceNone').click()
		else:
			# chapterNum = tdu.digits(chapter)-1
			# op.ChapterButtons.clickChild(chapterNum)
			op.ScrubberFadeButton.click(0)

	def ShowChapterButtons(self):
		currentRoom = ''

	def PauseLightingCue(self, currentVal):
		lightCue = op.GUI.fetch('LightingCues', {})
		currentRoom = op.GUI.fetch('CURRENT_ROOM')

		currentCue = lightCue.get(currentRoom, 0)
		if int(currentVal):
			op.COMM.op('oscout1').sendOSC('/hog/playback/halt/', [currentCue])
			print('Current Cue Paused = {}'.format(currentCue))

		else:
			op.COMM.op('oscout1').sendOSC('/hog/playback/resume/', [currentCue])

		# op.GUI.store('LIGHTS_PAUSED', True)

	def AudioLevels(self, cVal):
		audioLevelDict = op.GUI.fetch('AUDIO_LEVELS', {})
		room = op.GUI.fetch('CURRENT_ROOM')
		audioLevelDict[room] = cVal
		op.GUI.store('AUDIO_LEVELS', audioLevelDict)

	def ShowAudioControl(self, state=0):
		if state == 0:
			op.GUI.op('familyHub/interactiveMap/audio').par.tz = -10

		else:
			op.GUI.op('familyHub/interactiveMap/audio').par.tz =4
		pass


	def RecallPreset(self, num):
		print(num)
		button = op.Presets.op('radio')
		button.panel.celloverid = num-1
		button.clickID(num-1)

	def TabletFunks(self):
		'''
		"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions
import socket
import sys

class SamsungControl:
	"""
	SamsungControl description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp

		# attributes:
		self.a = 0 # attribute
		self.B = 1 # promoted attribute
		self.ContentDB = op.ContentDB
		# properties
		TDF.createProperty(self, 'MyProperty', value=0, dependable=True,
						   readOnly=False)

		# stored items (persistent across saves and re-initialization):
		storedItems = [
			# Only 'name' is required...
			{'name': 'StoredProperty', 'default': None, 'readOnly': False,
			 						'property': True, 'dependable': True},
		]
		# Uncomment the line below to store StoredProperty. To clear stored
		# 	items, use the Storage section of the Component Editor

		# self.stored = StorageManager(self, ownerComp, storedItems)

	def myFunction(self, v):
		debug(v)

	def PromotedFunction(self, v):
		debug(v)

	def GetControllerName(self):
		return socket.gethostname()

	def GetIPAddr(self):
		return socket.gethostbyname(self.GetControllerName())


	def SamsungClickEvent(self):
		pickPath = str(op.GUI.op('interact/pickPath')['path', 1])
		mainPage = op.GUI.op('interact').fetch('MAIN_PAGES', [])
		# sceneName = pickPath.split('/')[2]
		screenName = op.GUI.fetch('ACTIVE_SCREEN', None)
		print(pickPath)
		buttonName = pickPath.split('/')[-2]
		settingsScreen = op.GUI.fetch('SettingsScreen', None)

		roomNames = ['chefsCollection', 'dacor', 'familyHub']


		actions = ['pause', 'jump']
		print(buttonName)
		if buttonName in actions:
			if buttonName == 'pause':
				cState = op.GUI.fetch('PAUSE', 0)
				cVal = 1-cState
				op.GUI.store('PAUSE', cVal)
				self.PauseLightingCue(cVal)

				op.COMM.op('udpout1').send('{} {} {}'.format(op.GUI.fetch('ACTIVE_SCREEN'), buttonName.capitalize(), str(cVal)))
			elif buttonName == 'jump':
				op.COMM.op('udpout1').send('{} {} {}'.format(op.GUI.fetch('ACTIVE_SCREEN'), buttonName.capitalize(), (-5)))

		if buttonName in roomNames:
			buttonNum = roomNames.index(buttonName)
			op.RoomSelector.clickChild(buttonNum)

		if settingsScreen:
			name = settingsScreen
			op.GUI.store('ACTIVE_SCREEN', name)
			op.GUI.store('SettingsScreen', None)
		elif buttonName in mainPage:
			op.GUI.store('ACTIVE_SCREEN', buttonName)
		# switch(screenName)

		elif 'Settings' in buttonName:
			self.OpenSettingsScreen(screen=screenName, button=buttonName)

		elif 'chapter' in buttonName:
			self.SwitchContent(screen=screenName, button=buttonName)


		else:

			print(screenName, buttonName)



	def SwitchContent(self, button=None, screen=None):

		#	print(me, 'ACTIVE_SCREEN', screenName, buttonName)


		familyHubButtons = list(range(0, 14, 2))
		dacorButtons = list(range(6))
		chefsCollection = list(range(0, 9, 2))

		print(screen)
		# if screen == 'familyHu'
		# # print(sys._getframe().f_code.co_name)
		if 'family' in screen:
			activeList = familyHubButtons
		elif 'acor' in screen:
			activeList = dacorButtons

		elif 'ollection' in screen:
			activeList = chefsCollection


		buttonNum = tdu.digits(button)
		clickButton = activeList[buttonNum]
		print(button)
		room = op.GUI.fetch('ACTIVE_SCREEN', None)
		print(room, button)
		# activeDAT = self.ContentDB.op(screen + 'Active')
		#
		# activeDAT.par.dat = screen + 'Chapter' + str(buttonNum)


#		print(clickButton, 'This is the button numb')
#		op.Playlist.SelectRow(clickButton)
		msg = '{} {}'.format(room, button)
		op.COMM.op('lightCueManager').run(button, screen)
		op.COMM.op('udpout1').send(msg)
		return

	def OpenSettingsScreen(self, button=None, screen=None):
		# load settings into settings page
		settingsPage = button.split('Settings')
		op.GUI.op('settings/interactiveMap/setup').run(screen)

		# print(button, screen, settingsDat)
		# self.SwitchContent(button=button, screen=screen)
		op.GUI.store('ACTIVE_SCREEN', 'settings')
		op.GUI.store('SettingsScreen', screen)

	def SendLightingCue(self, cueNum=None, button=None, screen=None):

		pass

	def PauseLightingCue(self, currentVal):
		lightCue = op.GUI.fetch('LightingCues')
		currentRoom = op.GUI.fetch('ACTIVE_SCREEN')

		currentCue = lightCue.get(currentRoom)
		if int(currentVal):
			op.COMM.op('oscout1').sendOSC('/hog/playback/halt/', [currentCue])
			print('Current Cue Paused = {}'.format(currentCue))

		else:
			op.COMM.op('oscout1').sendOSC('/hog/playback/resume/', [currentCue])

		op.GUI.store('LIGHTS_PAUSED', True)


		:return:
		'''
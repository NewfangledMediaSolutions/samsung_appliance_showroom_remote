"""
Extension classes enhance TouchDesigner components with python. An
extension is accessed via ext.ExtensionClassName from any operator
within the extended component. If the extension is promoted via its
Promote Extension parameter, all its attributes with capitalized names
can be accessed externally, e.g. op('yourComp').PromotedFunction().

Help: search "Extensions" in wiki
"""

from TDStoreTools import StorageManager
TDF = op.TDModules.mod.TDFunctions
import socket
import sys

class Communications:
	"""
	SamsungControl description
	"""
	def __init__(self, ownerComp):
		# The component to which this extension is attached
		self.ownerComp = ownerComp
		self.CommDBDAT = ownerComp.op('communicationsDB')
		self.MoviePlayerUDP = ownerComp.op('videoPlayerUDP')
		# attributes:
		self.a = 0 # attribute
		self.B = 1 # promoted attribute
		# self.ContentDB = op.ContentDB
		# properties
		TDF.createProperty(self, 'MyProperty', value=0, dependable=True,
						   readOnly=False)

		# stored items (persistent across saves and re-initialization):
		storedItems = [
			# Only 'name' is required...
			{'name': 'StoredProperty', 'default': None, 'readOnly': False,
			 						'property': True, 'dependable': True},
		]
		# Uncomment the line below to store StoredProperty. To clear stored
		# 	items, use the Storage section of the Component Editor
		
		# self.stored = StorageManager(self, ownerComp, storedItems)

	def myFunction(self, v):
		debug(v)

	def PromotedFunction(self, v):
		debug(v)

	def SwitchCommPorts(self, roomName):
		dbHeaders = 'Target Type Port direction Control ip path	notes'
		headers = dbHeaders.split()
		tab = self.CommDBDAT
		if roomName != 'mainMenu':
			for i in self.CommDBDAT.rows()[1:]:
				if i[0].val == roomName:
					ip = tab[i[0].row, headers[5]]
					node = tab.parent().op(tab[i[0].row, headers[6]].val)
					port = tab[i[0].row, headers[2]].val
					if ip:
						node.par.address = ip
					if port:
						node.par.port = port

		return 



	def GetControllerName(self):
		return socket.gethostname()

	def GetIPAddr(self):
		return socket.gethostbyname(self.GetControllerName())



	def SwitchContent(self, button=None, screen=None):

		#	print(me, 'ACTIVE_SCREEN', screenName, buttonName)


		familyHubButtons = list(range(0, 14, 2))
		dacorButtons = list(range(6))
		chefsCollection = list(range(0, 9, 2))

		# print(screen)
		if 'family' in screen:
			activeList = familyHubButtons
		elif 'acor' in screen:
			activeList = dacorButtons

		else:
			activeList = chefsCollection


		buttonNum = tdu.digits(button)
		clickButton = activeList[buttonNum]
		room = op.GUI.fetch('ACTIVE_SCREEN', None)

		msg = '{} {}'.format(room, button)
		op.COMM.op('lightCueManager').run(button, screen)
		op.COMM.op('udpout1').send(msg)
		# self.ShowVideoScrubber(button)
		return
		
	def SendLightingCue(self, cueNum=None, button=None, screen=None):
		
		pass
